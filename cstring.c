#include <stdio.h>
#include <stdlib.h>
#include "cstring.h"

struct string *str_new_len(const char *str, int len)
{
	struct string *s = malloc(sizeof(struct string));
	s->len = len;
	s->cap = DATSZ;
	while (s->cap < s->len + 1)
		s->cap *= 2;  /* Adjust size */
	s->str = malloc(sizeof(char) * s->cap);
	strncpy(s->str, str, s->len);
	s->str[s->len] = 0; /* Fix end of string */
	return s;
}

struct string *str_new_from_file(const char *filename)
{
	struct string *s;
	FILE *fin = fopen(filename, "r");
	if (!fin)
	{
		fprintf(stderr, "str_new_from_file: file can't be open\n");
		exit(EXIT_FAILURE);
	}
	s = str_new_from_stream(fin);
	fclose(fin);
	return s;	
}

struct string *str_new_from_stream(FILE *stream)
{
	char line[BUFSZ + 1];
	struct string *s = str_new("");
	while (fgets(line, BUFSZ, stream) != NULL)
	{
		s->len += strlen(line);
		if (s->len >= s->cap)
		{
			char *aux;
			while (s->len >= s->cap)
				s->cap *= 2;
			aux = realloc(s->str, sizeof(char) * s->cap);
			s->str = aux;
		}
		strcat(s->str, line);
	}
	return s;	

}

int str_replace(struct string *s, char *pattern, char *replacement)
{
	struct string *r = str_new("");
	char *remain = s->str;
	char *pos;
	char segment[BUFSZ + 1];
	while ((pos = strstr(remain, pattern)) != NULL)
	{
		strncpy(segment, remain, pos - remain); 	
		segment[pos - remain] = 0;
		str_append(r, segment);
		str_append(r, replacement);
		remain = pos + strlen(pattern);
	}
	str_append(r, remain);
	pos = s->str;
	s->str = r->str;
	r->str = pos;
	str_free(r);
	return 1;
}

char *str_free_opt(struct string *s, int all)
{
    char *ret = s->str;
    if (all) {
        free(s->str);
        ret = NULL;
    }
    free(s);
    return ret;
}

struct string *str_erase_len(struct string *s, int pos, int len) {
    int i = pos;
    if (pos + 1 >= s->len)
        return s;
    while (s->str[i] && ((i + len) < s->len)) {
        s->str[i] = s->str[i + len];
        i++;
    }
    s->str[i] = 0;
    return s;
}

struct string *str_insert_len(struct string *s, int pos, char *str, int len)
{
    int i, modmem = 0;
    s->len += len;
    while (s->cap < (s->len + 1))
    {
        s->cap *= 2;
        modmem = 1;
    }
    if (modmem) {
        char *aux;
        aux = realloc(s->str, sizeof(char) * s->cap);
        s->str = aux;
    }
    for (i = s->len - len; i >= pos; i--)
        s->str[i + len] = s->str[i];
    strncpy(&(s->str[pos]), str, len);
    return s;
}

char **strsplit(char *st, const char *del) {
    char **ret = malloc(sizeof(char *) * DATSZ);
    char *ini, *end, **aux;
    int n, len, cap = DATSZ;
    for (n = 0, ini = st; 
        (end = strstr(ini, del)); 
        ini = end + strlen(del), n++) {
        if (n + 2 == cap) {
            cap *= 2;
            aux = realloc(ret, sizeof(char *) * cap);
            ret = aux;
        }
        len = end - ini;
        ret[n] = malloc(sizeof(char) * (len + 1));
        strncpy(ret[n], ini, len);
        ret[n][len] = 0;
    }
    ret[n] = malloc(sizeof(char) * (strlen(ini) + 1));
    strcpy(ret[n], ini);
    ret[n + 1] = NULL;
    return ret;
}

int charv_len(char **lst) {
    char **t; /* Iterador del vector de string */
    int n;    /* Variable para contar el número de elementos */
    for (t = lst, n = 0; *t; n++, t++);
    return n;
}

void charv_free(char **lst) {
    char **t;
    for (t = lst; *t; t++) free(*t);
    free(lst);
}

char *strdup(const char *s)
{
	char *ret = malloc(sizeof(char) * (strlen(s) + 1));
	strcpy(ret, s);
	return ret;
}
