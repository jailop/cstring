# cstring - a minimal extension for C strings

I think that string functions from the standard C library, with memory
allocation functions & pointer operations, are a powerful tool set. But, we
frequently need some extensions, like automatically manage memory allocation
with concatenate operation to permit safe-growing strings, a pattern replace
function and some other hacks. So, this is a little library that extends the
standard string functions using the following structure:

    struct string
    {
    	char *str; /* String */
    	int   len; /* Length - space used */
    	int   cap; /* Capacity */
    };

It is a standard string that record its length and remember how many memory it
has assigned. Therefore, if you need concatenate to it another string, it checks
if has enough memory to accept the new one, allocating more space if is
necessary.

## Contact

Jaime López <jailop AT gmail.com>
