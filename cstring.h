#ifndef _CSTRING_H
#define _CSTRING_H 1

#include <string.h>

#ifndef BUFSZ
#define BUFSZ 4096  /* Default size for character buffers */
#endif

#ifndef DATSZ
#define DATSZ 64    /* Initial size of allocated memory for data */
#endif

/* string is a struct that holds autogrowing strings. It has three fields:
 * `str` allocates the string; `len` indicates the string length at current
 * moment; and `cap` is the current maximum capacity of the string.
 */

struct string
{
    char *str; /* String */
    int   len; /* Length - space used */
    int   cap; /* Capacity */
};

/* str_new_len creates a new string with an initial length.
 *
 * @params st : Source string. It can be empty.
 * @params len : Initial length
 *
 * @retun A new string.
 */

struct string *str_new_len(const char *st, int len);

/* str_new creates a new string with the length of the string given as
 * parameter. It is a macro to str_new_len. The initial length is calculated
 * with strlen.
 *
 * @params st : Source string. It can be empty.
 *
 * @return A new string
 */

#define str_new(st) str_new_len((st), strlen(st))

/* str_new_from_file creates a new string from an input file. It uses the
 * str_new_from_stream function adding the operations to open and close the
 * file.
 *
 * @params filename: The input filename
 *
 * @return: A new string
 */

struct string *str_new_from_file(const char *filename);

/* str_new_from_stream creates a new string from an input stream. It may be a
 * file or other stream of length not previously know.
 * 
 * @params stream: An input stream.
 *
 * @return: A new string
 */

struct string *str_new_from_stream(FILE *stream);

int str_replace(struct string *s, char *pattern, char *replacement);

/* str_free_opt liberates space used in memory by a struct string structure. If
 * `all` parameter is set to 1 then memory allocate for `str` is free too, else
 * memory is not touched. This is useful when `str` has been allocated by others
 * functions and they manages its deallocation.
 *
 * @params s : string structure to be deallocated
 * @params all : if 1 then memory assigned to str field is deallocated
 *
 * @return NULL is all = 1, else a pointer to the str field.
 */

char *str_free_opt(struct string *s, int all);

#define str_free(s) str_free_opt(s, 1)

struct string *str_erase_len(struct string *s, int pos, int len);

#define str_erase(s, pos) str_erase_len((s), (pos), strlen(s->str) - (pos))

struct string *str_insert_len(struct string *s, int pos, char *st, int len);

#define str_insert(s, pos, st) str_insert_len((s), (pos), (st), strlen(st))

#define str_append(s, st) str_insert_len((s), strlen(s->str), (st), strlen(st))

#define str_prepend(s, st) str_insert_len((s), 0, (st), strlen(st))

int charv_len(char **lst);

void charv_free(char **lst);

char **strsplit(char *st, const char *del);

char *strdup(const char *s);

#endif /* _CSTRING_H */ 
