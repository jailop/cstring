#include "carray.h"
#include <stdlib.h>
#include <stdio.h>

int main(void)
{
	int ret = 1;
	int i;
	long int vals[] = {31, 28, 25, 0, -33};
	struct array *da = array_new(NULL);
	for (i = 0; i < 3; i++)
		array_append(da, (void *) (vals[i]));
	for (i = 0; i < da->len; i++)
		ret *= vals[i] == (long int) array_get(da, i);
	array_free(da);

	return !ret;
}
