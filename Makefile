OUT = libcstring.so
HDR = cstring.h carray.h
BIN = carray_test
OBJ = cstring.o carray.o
CFLAGS = -Wall -ansi -pedantic -g -fpic -O3
LDLIBS = 
PATH_INSTALL = /usr/local

all: $(OUT) $(BIN)

$(BIN): $(OBJ)

$(OUT): $(OBJ)
	$(CC) -shared $(LDLIBS) -o $(OUT) $(OBJ)

install: $(OUT) install_hdr
	mv $< $(PATH_INSTALL)/lib
	ldconfig

install_hdr: $(HDR)
	cp $< $(PATH_INSTALL)/include

deinstall:
	rm -f $(PATH_INSTALL)/lib/$(OUT)
	rm -f $(PATH_INSTALL)/include/$(HDR)

clean:
	rm -f $(OBJ)
	rm -f $(OUT)
